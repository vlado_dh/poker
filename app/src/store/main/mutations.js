export function SET_GAMESITUATION(state, payload) {
  state.gameSituation = payload
}

export function SET_BROWSERDATA(state, payload) {
  state.browserData = payload
}

export function SET_GRIDFOLDLABEL(state, payload) {
  state.browsercolumns.map((row) => {
    if (row.name == 'fold_name') {
      row.label = payload.label
      if (payload.percent != 0) {
        row.percent = payload.percent.toFixed(1) + '%'
      }
    }
  })
}

export function SET_GRIDCALLLABEL(state, payload) {
  state.browsercolumns.map((row) => {
    if (row.name == 'call_name') {
      row.label = payload.label
      if (payload.percent != 0) {
        row.percent = payload.percent.toFixed(1) + '%'
      }
    }
  })
}

export function SET_GRIDRAISELABEL(state, payload) {
  state.browsercolumns.map((row) => {
    if (row.name == 'raise_name') {
      row.label = payload.label
      if (payload.percent != 0) {
        row.percent = payload.percent.toFixed(1) + '%'
      }
    }
  })
}

export function SET_GRIDALLINLABEL(state, payload) {
  state.browsercolumns.map((row) => {
    if (row.name == 'allin_name') {
      row.label = payload.label
      if (payload.percent != 0) {
        row.percent = payload.percent.toFixed(1) + '%'
      }
    }
  })
}
export function SET_POTISZE(state, payload) {
  console.log('Set potSize to: ', payload)
  state.potSize = payload
}

export function SET_VISIBLECOLS(state, payload) {
  state.visiblecolumns = payload
}

export function SET_STACK_FORMAT(state, payload) {
  state.browserSettings.stackFormat = payload
}

export function SET_BROWSER(state, payload) {
  let filtered = payload.vCols.filter((one) => {
    return one.match(/^.+_comb$/) === null
  })
  state.visiblecolumns = filtered
  state.browserdata = payload.dataSource
}

export function SET_BROWSER_MORE(state, payload) {
  console.log('SET_BROWSER_MORE', payload)
  for (let i in payload) {
    let row = payload[i]
    state.browserdata.push(row)
  }
}

export function SET_PLAYER_PROPERTIES(state, name, props) {
  var players = state.gameScenario.players
  var lastIdx = players[players.length - 1].idx
  players.map((player) => {
    if (player.name == name) {
      for (const [prop, value] of Object.entries(props)) {
        player[prop] = value
        // if folded before
        if (prop == 'action' && value == 'F') {
          player.fold = false
          player.active = true
        }
        // player bet back to stack
        // var tempstack = player.stack
        player.stack = props.stack // tempstack + props.bet
        player.bet = props.bet // state.browserSettings.stack - (tempstack + props.bet)
      }
    }
    player.idx = lastIdx++
  })
}

export function recalculatePotSize(state) {
  var players = state.gameScenario.players
  // eslint-disable-next-line no-unused-vars
  var oldPot = state.potSize
  // eslint-disable-next-line no-unused-vars
  players.map((player) => {
    // prepocet stavky
  })
}

export function setBrowserSettings(state, payload) {
  Object.entries(payload).forEach(([key, val]) => {
    state.browserSettings[key] = val
  })
  // if (typeof payload === 'object') state.browserSettings = payload
  // else {
  //   Object.entries(payload).forEach(([key, val]) => {
  //     state.browserSettings[key] = val
  //   })
  // }
}

export function setTrainingSettings(state, payload) {
  Object.entries(payload).forEach(([key, val]) => {
    state.trainingSettings[key] = val
  })
}

export function setSituationKey(state, payload) {
  state.gameScenario.situationKey = payload
  console.log('state.situatonKey:', state.gameScenario.situationKey)
  setSituationText(state)
}

export function setSituationText(state) {
  var text = ''
  var order = 0
  var curency = 'BB'
  var money
  state.gameScenario.story.map((story) => {
    if (order > 2) {
      money = story.pot + ' ' + curency
      text += ' - ' + story.player + ' '
      if (story.action == 'R100') {
        text += 'raise ' + money
      } else if (story.action == 'R50') {
        text += 'raise ' + money
      } else if (story.action == 'R33') {
        text += 'raise ' + money
      } else if (story.action == 'R25') {
        text += 'raise ' + money
      } else if (story.action == 'A') {
        text += 'allin ' + money
      } else if (story.action == 'C') {
        text += 'call ' + money
      } else if (story.action == 'F') {
        text += 'fold'
      } else {
        text += story.action + ' ' + story.pot
      }
    }
    order++
  })
  state.gameScenario.situationText = text
}

export function SET_ACTIVE_PLAYER(state, name) {
  console.log('Activate player: ' + name)
  var maxi = state.gameScenario.players[5].idx
  var activePlayer = -1

  // set all player to inactive
  state.gameScenario.players.map((p) => {
    p.active = false
  })

  // set current active player
  // .filter((player) => !player.fold)
  state.gameScenario.players.map((player) => {
    player.idx = maxi++
    // player.bet = Math.round((player.bet + 0.1)*10 )/10
    if (player.name == name) {
      player.active = true
      player.fold = false // getNewPlayerIndex() included fold logic
      activePlayer = player
    }
  })
  return activePlayer
}

export function SET_FOLDED_PLAYER(state) {
  var players = state.gameScenario.players
  var lastIdx = players[players.length - 1].idx
  state.gameScenario.players.map((player) => {
    player.idx = lastIdx++
    if (player.active === true) {
      player.fold = true
      state.potSize += player.bet
      player.bet = 0
    }
  })
}

export const ACTIVATE_PLAYER = (state, name) => {
  let positions = []
  let oldPosition = 0
  let currentActive = 0
  let rotateIndex = 0
  const rotate = (arr, count) => {
    if (count === 0) return JSON.parse(JSON.stringify(arr))
    return [...arr.slice(count, arr.length), ...arr.slice(0, count)]
  }

  // set active player
  state.gameScenario.players.map((player) => {
    positions.push(player.position)
    if (player.name == name) {
      oldPosition = player.position
      player.active = true
    }
  })
  currentActive = positions.indexOf('bc')
  // rotate positions
  rotateIndex = currentActive - positions.indexOf(oldPosition)
  positions = rotate(positions, rotateIndex)

  state.gameScenario.players.map((player, index) => {
    player.position = positions[index]
  })
}

export function INC_HANDSTEP(state) {
  state.currentHand += 1
}

export function DEC_HANDSTEP(state) {
  state.currentHand -= 1
}
