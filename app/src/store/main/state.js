function formatFreq(v) {
  if (typeof v === 'string' && v.length) return (v * 100).toFixed(1) + '%'
  return ''
}
function formatEv(v) {
  return v == '-0.0' ? '0.0' : v
}

export default {
  gameSituation: 'preflop',
  currentHand: 1,
  potSize: 0,
  swVer: '1.0',
  totalHands: 20,
  playerPositions: ['bc', 'br', 'tr', 'tc', 'tl', 'bl'],
  newPlayerPositions: ['bc', 'bl', 'tl', 'tc', 'tr', 'br'],
  browserTableSlots: ['body-cell-fold_name', 'body-cell-call_name', 'body-cell-raise_name'],
  browserSettings: {
    profile: 'PLO500',
    stack: '100',
    numberOfPlayers: 6,
    searchState: null,
    searchInput: null,
    searchInDs: null,
    searchInDd: null,
    searchInRr: null,
    selectedCategory: null,
    stackFormat: 'bb'
  },
  trainingSettings: {
    profile: null,
    hands: 1,
    scenario: null
  },
  gameScenario: {
    situationKey: '-',
    situationText: '-',
    defaultSituation: '-',
    story: [
      {
        player: 'SB',
        hand: 1,
        action: 'ps',
        bet: 1,
        stack: 0,
        pot: 0,
        status: 'default',
        stuation: ''
      },
      {
        player: 'BB',
        hand: 1,
        action: 'ps',
        bet: 2,
        stack: 0,
        pot: 1,
        status: 'default',
        stuation: ''
      },
      {
        player: 'EP',
        hand: 1,
        action: '',
        bet: 0,
        stack: 0,
        pot: 0,
        status: 'begin',
        stuation: '-'
      }
    ],
    buttons: [
      {
        id: 'fold',
        label: 'Fold',
        visible: true
      },
      {
        id: 'call',
        label: 'Call',
        visible: true
      },
      {
        id: 'raise50',
        label: 'Raise 50%',
        visible: true
      },
      {
        id: 'raise100',
        label: 'Raise 100%',
        visible: true
      },
      {
        id: 'allin',
        label: 'Allin',
        visible: false
      },
      {
        id: 'back',
        label: '<',
        visible: false
      }
    ]
  },
  browserdatadefault: {
    id: 0,
    fold_name: '',
    fold_frekv: '',
    fold_ev: '',
    call_name: '',
    call_frekv: '',
    call_ev: '',
    raise_name: '',
    raise_frekv: '',
    raise_ev: '',
    allin_name: '',
    allin_frekv: '',
    allin_ev: ''
  },
  browserdata: [],
  visiblecolumns: [
    'fold_name',
    'fold_frekv',
    'fold_ev',
    // 'fold_comb',
    'call_name',
    'call_frekv',
    'call_ev',
    // 'call_comb',
    'raise_name',
    'raise_frekv',
    'raise_ev',
    // 'raise_comb',
    'allin_name',
    'allin_frekv',
    'allin_ev'
    // 'allin_comb'
  ],
  browsercolumns: [
    {
      name: 'fold_name',
      required: false,
      label: 'Fold',
      align: 'center',
      field: (row) => row.fold_name,
      format: (val) => `${val}`,
      sortable: false,
      percent: 0,
      classes: 'fold_column',
      visible: true
    },
    {
      name: 'fold_frekv',
      label: 'Freq',
      required: false,
      sortable: false,
      field: (row) => row.fold_frekv,
      format: (val) => (val > 0 ? (val * 100).toFixed(1) + '%' : ''),
      classes: 'params',
      visible: true
    },
    {
      name: 'fold_ev',
      label: 'Ev',
      required: false,
      sortable: false,
      field: (row) => row.fold_ev,
      format: (val) => `${val}`,
      classes: 'params',
      visible: true
    },
    {
      name: 'fold_comb',
      label: 'Comb',
      required: false,
      sortable: false,
      field: (row) => row.fold_comb,
      format: (val) => (val > 0 ? parseInt(`${val}`) : ''),
      classes: 'params',
      visible: false
    },
    {
      name: 'call_name',
      required: false,
      label: 'Call',
      align: 'center',
      field: (row) => row.call_name,
      format: (val) => `${val}`,
      sortable: false,
      percent: 0,
      classes: 'call_column',
      visible: true
    },
    {
      name: 'call_frekv',
      label: 'Freq',
      required: false,
      field: (row) => row.call_frekv,
      format: (val) => (val > 0 ? (val * 100).toFixed(1) + '%' : ''),
      sortable: false,
      classes: 'params',
      visible: true
    },
    {
      name: 'call_ev',
      label: 'Ev',
      required: false,
      field: (row) => row.call_ev,
      format: (val) => `${val}`,
      sortable: false,
      classes: 'params',
      visible: true
    },
    {
      name: 'call_comb',
      label: 'Comb',
      required: false,
      sortable: false,
      field: (row) => row.call_comb,
      format: (val) => (val > 0 ? parseInt(`${val}`) : ''),
      classes: 'params',
      visible: false
    },
    {
      name: 'raise_name',
      required: false,
      label: 'Raise',
      align: 'center',
      field: (row) => row.raise_name,
      format: (val) => `${val}`,
      sortable: false,
      percent: 0,
      classes: 'raise_column',
      visible: true
    },
    {
      name: 'raise_frekv',
      label: 'Freq',
      required: false,
      sortable: false,
      field: (row) => row.raise_frekv,
      format: (val) => formatFreq(val), //(val > 0 ? (val * 100).toFixed(1) + '%' : val <= 0 ? '0%' : ''),
      classes: 'params',
      visible: true
    },
    {
      name: 'raise_ev',
      label: 'Ev',
      required: false,
      sortable: false,
      field: (row) => row.raise_ev,
      format: (val) => formatEv(val), //`${val}`,
      classes: 'params',
      visible: true
    },
    {
      name: 'raise_comb',
      label: 'Comb',
      required: false,
      sortable: false,
      field: (row) => row.raise_comb,
      format: (val) => (val > 0 ? parseInt(`${val}`) : ''),
      classes: 'params',
      visible: false
    },
    {
      name: 'allin_name',
      required: false,
      label: 'AllIn',
      align: 'center',
      field: (row) => row.allin_name,
      format: (val) => `${val}`,
      sortable: false,
      percent: 0,
      classes: 'allin_column',
      visible: true
    },
    {
      name: 'allin_frekv',
      label: 'Freq',
      required: false,
      sortable: false,
      field: (row) => row.allin_frekv,
      format: (val) => (val > 0 ? (val * 100).toFixed(1) + '%' : ''),
      classes: 'params',
      visible: true
    },
    {
      name: 'allin_ev',
      label: 'Ev',
      required: false,
      sortable: false,
      field: (row) => row.allin_ev,
      format: (val) => `${val}`,
      classes: 'params',
      visible: true
    },
    {
      name: 'allin_comb',
      label: 'Comb',
      required: false,
      sortable: false,
      field: (row) => row.allin_comb,
      format: (val) => (val > 0 ? parseInt(`${val}`) : ''),
      classes: 'params',
      visible: false
    }
  ],
  pokerCards: [
    ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2'],
    ['s', 'h', 'd', 'c']
  ],
  pokerPlayers: ['BB', 'SB', 'BTN', 'CO', 'MP', 'EP'],
  chipValues: [100, 50, 20, 10, 5, 2, 1, 0.5, 0.25],
  /* for future */
  finishHands: [
    {
      regex: /(2345A|23456|34567|45678|56789|6789T|789JT|89JQT|9JKQT|AJKQT)#(.)\2{4}.*/g,
      name: 'Straight flush'
    },
    {
      regex: /(.)\1{3}.*#.*/g,
      name: 'Four of a kind'
    },
    {
      regex: /((.)\2{2}(.)\3{1}#.*|(.)\4{1}(.)\5{2}#.*)/g,
      name: 'Full house'
    },
    {
      regex: /.*#(.)\1{4}.*/g,
      name: 'Flush'
    },
    {
      regex: /(2345A|23456|34567|45678|56789|6789T|789JT|89JQT|9JKQT|AJKQT)#.*/g,
      name: 'Straight'
    },
    {
      regex: /(.)\1{2}.*#.*/g,
      name: 'Three of a kind'
    },
    {
      regex: /(.)\1{1}.*(.)\2{1}.*#.*/g,
      name: 'Two pair'
    },
    {
      regex: /(.)\1{1}.*#.*/g,
      name: 'One pair'
    }
  ]
}
