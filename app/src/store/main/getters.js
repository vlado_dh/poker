export default {
  browserData: function(state) {
    return state.browserdata
  },
  browserCols: function(state) {
    return state.browsercolumns
  },
  browserSettings: function(state) {
    return state.browserSettings
  },
  stack: function(state) {
    return state.browserSettings.stack
  },
  stackFormat: function(state) {
    return state.browserSettings.stackFormat
  },
  browserProfile: function(state) {
    return state.browserSettings.profile
  },
  visibleCols: function(state) {
    return state.visiblecolumns
  },
  pokerHands: function(state) {
    return state.pokerCards.reduce((a, b) =>
      a.reduce((r, v) => r.concat(b.map((w) => [].concat(v, w))), []).map((a) => a.join(''))
    )
  },
  gameSituation: function(state) {
    return state.gameSituation
  },
  gameScenario: function(state) {
    return state.gameScenario
  },
  handCounter: function(state) {
    return state.currentHand + '/' + state.totalHands
  },
  potSize: function(state) {
    return state.potSize
  },
  betSize: function(state) {
    let betSize = 0
    state.gameScenario.players
      .filter((player) => player.fold == false)
      .map((p) => (betSize += p.bet))
    return betSize
  },
  chipValues: function(state) {
    return state.chipValues
  },
  getBrowserButtonById: (state) => (id) => {
    return state.gameScenario.buttons.find((button) => button.id === id)
  },
  getActivePlayer: function(state) {
    return state.gameScenario.players.find((player) => player.active == true)
  },
  getActivePlayerIndex: function(state) {
    return state.gameScenario.players.findIndex((idx) => idx.active == true)
  }
}
