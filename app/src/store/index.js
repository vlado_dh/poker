import Vue from 'vue'
import Vuex from 'vuex'
import feathersVuex from 'feathers-vuex'
import feathers from '../api/feathers' // Cannot load this as a plugin :-(

import main from './main'
import browser from './browser'
import situations from './situations'
import browserGrid from './browser-grid'

const { service, auth } = feathersVuex(feathers, {
  idField: '_id', // The field in each record that will contain the id
  autoRemove: false, // automatically remove records missing from responses (only use with feathers-rest)
  nameStyle: 'short', // Determines the source of the module name. 'short' or 'path'
  enableEvents: true // Set to false to explicitly disable socket event handlers.
})
const app = feathers
Vue.use(Vuex)
//Vue.use(FeathersVuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      position: 'F-F-F',
      cards: [],
      situations: []
    },
    modules: {
      main,
      browser,
      situations,
      browserGrid
    },
    mutations: {
      GET_CARDS_DATA(state, $cards) {
        state.cards = $cards
      }
    },
    actions: {
      situations: async function (state, dataSet) {
        const dataBrowse = []
        const {
          preflop_code,
          players_cnt,
          profile,
          stack,
          showZeroRows,
          page,
          currentHands,
          situationId,
          rawhand,
          category,
          ss,
          ds,
          rainbow
        } = dataSet
        if (page) {
          for (var action in currentHands) {
            let ac = currentHands[action]
            if (!ac) continue
            const findParPD = {
              query: {
                situation_id: situationId,
                action: action.toUpperCase(),
                $sort: { ev: -1, weight: -1 },
                $limit: 20,
                $skip: page,
                $select: ['hand', 'weight', 'ev', 'action', 'interesting', 'combinations']
              }
            }

            if (!showZeroRows) findParPD.query.weight = { $gte: 0.05 } // findParPD.query.weight = { $ne: 0 }
            if (typeof rawhand !== 'undefined' && rawhand)
              findParPD.query.rawhand = { $like: '%' + rawhand + '%' }
            if (category) findParPD.query.category = category
            if (typeof ss !== 'undefined' && ss) findParPD.query.ss = ss ? true : null
            if (typeof ds !== 'undefined' && ds) findParPD.query.ds = ds ? true : null
            if (typeof rainbow !== 'undefined') findParPD.query.rainbow = rainbow ? true : null

            const preflopHandsDataActionP = app.service('preflop-hands').find(findParPD) // v-preflop-hands
            const preflopHandsDataAction = await preflopHandsDataActionP.then(
              (preflopHandsDataAction) => {
                return preflopHandsDataAction
              }
            )
            // console.log('d:',preflopHandsDataAction)
            dataBrowse.push({ action: row, data: preflopHandsDataAction.data })
          }
          return dataBrowse
        }

        const findParamsSD = {
          query: {
            preflop_code: preflop_code,
            players_cnt: players_cnt,
            profile: profile,
            stack: stack,
            postflop_code: null, // .. sure ?
            $select: ['id']
          }
        }

        const situationsData = app.service('situations').find(findParamsSD)
        var data = await situationsData.then((data) => {
          return data
        })
        if (!data.data.length) return [0, []]
        var situationId0 = data.data[0].id

        const findParamsPP = {
          query: {
            situation_id: situationId0
          }
        }
        const preflopPercentageData = app.service('preflop-percentage').find(findParamsPP)
        var dataPercentage = await preflopPercentageData.then((dataPercentage) => {
          return dataPercentage
        })

        var actionPercentage = []
        for (var row of dataPercentage.data) {
          const findParPD = {
            query: {
              situation_id: situationId0,
              action: row.action,
              $sort: { ev: -1, weight: -1 },
              $limit: 20,
              $select: ['hand', 'weight', 'ev', 'action', 'interesting', 'combinations']
            }
          }

          actionPercentage[row.action] = row.percent
          if (!showZeroRows) findParPD.query.weight = { $gte: 0.05 } // findParPD.query.weight = { $ne: 0 }
          if (typeof rawhand !== 'undefined' && rawhand)
            findParPD.query.rawhand = { $like: '%' + rawhand + '%' }
          if (category) findParPD.query.category = category
          if (typeof ss !== 'undefined' && ss) findParPD.query.ss = ss ? true : null
          if (typeof ds !== 'undefined' && ds) findParPD.query.ds = ds ? true : null
          if (typeof rainbow !== 'undefined') findParPD.query.rainbow = rainbow ? true : null

          const preflopHandsDataActionP = app.service('preflop-hands').find(findParPD) // v-preflop-hands
          const preflopHandsDataAction = await preflopHandsDataActionP.then(
            (preflopHandsDataAction) => {
              return preflopHandsDataAction
            }
          )
          // console.log(preflopHandsDataAction)
          let oneResult = { action: row, data: preflopHandsDataAction.data }
          if (preflopHandsDataAction.additional)
            oneResult.additional = preflopHandsDataAction.additional
          dataBrowse.push(oneResult)
        }

        // data sa odovzdaju komponente, ktora data upravi, cize state nemusime uptavit
        // kym sa udaje beru z komponenty, nie zo state
        /*
        var data = await preflopHandsData.then((data) => {return data})
        this.commit('GET_CARDS_DATA', data)
        */

        return [situationId0, dataBrowse, actionPercentage]
      },
      categories: async function (state, params) {
        const queryParams = {
          query: {
            rawhand: params.rawhand
          }
        }
        const promise = app.service('hand-categories').find(queryParams)
        let categories = await promise.then((result) => {
          return result
        })
        return categories
      }
    },
    plugins: [
      service('users'),
      service('hands'),
      service('situations'),
      service('preflop-hands'),
      service('hand-categories'),
      service('v-preflop-hands'),

      auth({
        userService: 'users'
      })
    ]
  })

  if (process.env.DEV) {
    window.$store = Store
  }

  return Store
}
