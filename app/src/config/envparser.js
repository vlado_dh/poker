// config/envparser.js

module.exports = function() {
  let parsedEnv = {}
  for (let key in process.env) {
    if (typeof process.env[key] === 'string') {
      parsedEnv[key] = JSON.stringify(process.env[key])
    }
  }
  return parsedEnv
}
