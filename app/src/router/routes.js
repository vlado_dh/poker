const routes = [
  {
    path: '/',
    component: () => import('layouts/PokerLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Index.vue')
      },
      {
        path: 'training',
        component: () => import('pages/Training.vue'),
        children: [
          {
            path: 'settings',
            name: 'trainingSettings',
            component: () => import('pages/TrainingSettings.vue')
          },
          {
            path: 'game',
            name: 'trainingGame',
            component: () => import('pages/TrainingGame.vue')
          }
        ]
      },
      {
        path: 'browser',
        component: () => import('layouts/BrowserLayout.vue'),
        children: [
          {
            path: '',
            name: 'browserDetail',
            component: () => import('pages/Browser.vue')
          },
          {
            path: 'settings',
            name: 'browserSettings',
            component: () => import('pages/BrowserSettings.vue')
          }
        ]
      },
      {
        path: 'settings',
        name: 'settingsPage',
        component: () => import('pages/Settings.vue')
      },
      {
        path: 'stats',
        name: 'statsPage',
        component: () => import('pages/Stats.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
