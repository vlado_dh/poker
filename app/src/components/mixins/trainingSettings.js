export default {
  data() {
    return {
      lib: {
        handsOptions: ['10', '25', '50', '100'],
        scenarioOptions: [
          'Random vs 1 Player',
          'RFI',
          'vs 1 Raise',
          'vs 3-Bet',
          'vs 4-Bet. Add Random vs 2+ players',
          'Vs raise + caller(s)',
          'Vs squeeze*',
          'Vs 4-bet after squeeze'
        ],
        positionOptions: ['Random', 'EP', 'MP', 'CO', 'BTN', 'SB', 'BB']
      }
    }
  },
  methods: {
    getChange: function() {
      let amount = this.potSize
      let chips = this.chipValues
      return chips.map((chip) => {
        let amountChip = Math.floor(amount / chip)
        amount -= amountChip * chip
        return [amountChip, chip]
      })
    },
    onlyNonZeroChips: function() {
      let chipArr = this.getChange
      chipArr.forEach((ch) => {
        if (ch[0] > 1) {
          for (let i = 1; i < ch[0]; i++) {
            chipArr.push([ch[0], ch[1]])
          }
        }
      })
      return chipArr.filter(function(ch) {
        return ch[0]
      })
    },
    getHandsOptions() {
      return this.lib.handsOptions
    },
    getScenarioOptions() {
      return this.lib.scenarioOptions
    },
    getPositionOptions() {
      return this.lib.positionOptions
    }
  }
}
