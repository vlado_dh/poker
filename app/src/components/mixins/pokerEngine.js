/* refactor code **/
class pokerPlayer {
  constructor(name, position, stack, showhand = false, active = false, fold = false) {
    this._name = name
    this._position = position
    this._stack = stack
    this._show = showhand
    this._active = active
    this._fold = fold
    this._bet = 0
    this._suit = null
    this._cards = ''
    this._positions = ['bc', 'bl', 'tl', 'tc', 'tr', 'br']
    this._idx = 0
  }

  get name() {
    return this._name
  }

  get bet() {
    return this._bet
  }

  get cards() {
    return this._cards
  }

  get fold() {
    return this._fold
  }

  get positionIndex() {
    return this.getPosition(this._position)
  }

  get position() {
    return this._position
  }

  get stack() {
    return this._stack
  }

  get showhand() {
    return this._show
  }

  // compatibility with old store definition
  get show() {
    return this._show
  }

  get isActive() {
    return this._active
  }

  // compatibility with old store definition
  get active() {
    return this._active
  }

  get idx() {
    return this._idx
  }

  set bet(size) {
    if (size <= this._stack) {
      // previous bet exists? put it back to stack
      if (this._bet > 0) {
        this._stack += this._bet
      }
      this._bet = size
      this._stack -= size
    }
  }

  set stack(stack) {
    this._stack = stack
  }

  set suit(cards) {
    this._suit = cards
  }

  set active(state) {
    this._active = state
  }

  set fold(state) {
    this._fold = state
  }

  set idx(id) {
    this._idx = id
  }

  incStack(value) {
    this._stack += value
  }

  decStack(value) {
    if (this._stack > value) {
      this._stack -= value
    }
  }

  getPosition() {
    return this._positions.indexOf(this._position)
  }

  nextPosition() {
    return this._positions.indexOf(this._position) == this._positions.length - 1
      ? 0
      : this._positions.indexOf(this._position) + 1
  }

  prevPosition() {
    return this._positions.indexOf(this._position) == 0
      ? this._positions.length - 1
      : this._positions.indexOf(this._position) - 1
  }
}

class pokerGame {
  constructor() {
    this.players = []
    this.playerNames = ['BTN', 'SB', 'BB', 'EP', 'MP', 'CO']
    this.playerPositions = ['bc', 'bl', 'tl', 'tc', 'tr', 'br']
    this.handColor = ['s', 'h', 'd', 'c']
    this.handCards = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2']
    this._potSize = 0
    this.gameStory = []
    this._idx = 0
  }

  get potSize() {
    return this._potSize
  }

  get maxBet() {
    let maxBet = 0
    this.players.map((player) => {
      if (!player.fold && player.bet > maxBet) {
        maxBet = player.bet
      }
    })
    return maxBet
  }

  get totalBets() {
    let total = 0
    this.players.map((player) => {
      if (!player.fold) {
        total += player.bet
      }
    })
    return total
  }

  get allPlayers() {
    return this.players
  }

  get activePlayer() {
    return this.players.find((player) => {
      return player.isActive
    })
  }

  get nextPlayer() {
    if (this.activePlayer) {
      let nextPosition = this.activePlayer.nextPosition()
      return this.players.find((player) => {
        return player.positionIndex == nextPosition
      })
    } else return false
  }

  get prevPlayer() {
    if (this.activePlayer) {
      let prevPosition = this.activePlayer.prevPosition()
      return this.players.find((player) => {
        return player.positionIndex == prevPosition
      })
    } else return false
  }

  get nextUnfoldedPlayer() {
    let player = this.nextPlayer
    while (player.fold == true) {
      player = this.getPlayerByPosition(
        this.playerPositions[this.forwardPlayerPosition(player.position)]
      )
    }
    return player
  }

  get prevUnfoldedPlayer() {
    let player = this.prevPlayer
    while (player.fold) {
      player = this.getPlayerByPosition(
        this.playerPositions[this.backwardPlayerPosition(player.position)]
      )
    }
    return player
  }

  set potSize(value) {
    this._potSize += value
  }

  resetPot() {
    this._potSize = 0
  }

  // init in Trainer section
  initGame(players = 6, stack = 200, position = 'EP', smallblind = 0.5, bigblind = 1) {
    this.players = []
    let pos = position
    if (pos == 'Random') {
      pos = this.playerPositions[Math.floor(Math.random() * this.playerPositions.length)]
    }
    for (let i = 0; i < players; i++) {
      this.createPlayer(this.playerNames[i], this.playerPositions[i], stack)
    }
    this.resetPot()
    this.setPlayerBet('SB', smallblind)
    this.setPlayerBet('BB', bigblind)
    this.activatePlayer(pos)
  }

  initBrowserGame(settingsObj) {
    this.players = []
    for (let i = 0; i < settingsObj.numberOfPlayers; i++) {
      this.createPlayer(this.playerNames[i], this.playerPositions[i], settingsObj.stack)
    }
    this.resetPot()
    this.setPlayerBet('SB', 0.5)
    this.setPlayerBet('BB', 1)
    this.activatePlayer('EP')
  }

  createPlayer(name, position, stack) {
    let p = new pokerPlayer(name, position, stack)
    this.players.push(p)
    return p
  }

  forwardPlayerPosition(currentPos) {
    return this.playerPositions.indexOf(currentPos) == this.playerPositions.length - 1
      ? 0
      : this.playerPositions.indexOf(currentPos) + 1
  }

  backwardPlayerPosition(currentPos) {
    return this.playerPositions.indexOf(currentPos) == 0
      ? this.playerPositions.length - 1
      : this.playerPositions.indexOf(currentPos) - 1
  }

  activatePlayer(name) {
    this.players.forEach((player) => {
      if (player.name == name) {
        player.active = true
      } else {
        player.active = false
      }
    })
    this.incIdx()
  }

  activateNextPlayer() {
    this.activatePlayer(this.nextPlayer.name)
  }

  activatePrevPlayer() {
    this.activatePlayer(this.prevPlayer.name)
  }

  getPlayerByName(name) {
    return this.players.find((player) => {
      return player.name == name
    })
  }

  getPlayerByPosition(pos) {
    return this.players.find((player) => {
      return player.position == pos
    })
  }

  incPot(value) {
    this._potSize += value
  }

  decPot(value) {
    if (this._potSize > value) {
      this._potSize -= value
    }
  }

  actionFold() {
    let ap = this.activePlayer
    let prevBet = ap.bet
    ap.fold = true
    this.incPot(ap.bet)
    ap.decStack(ap.bet)
    ap.bet = 0
    this.saveAction(ap.name, 'fold', ap.bet, ap.stack, this.potSize, 'Fold', prevBet)
    this.activateNextPlayer()
  }

  betToCall() {
    return this.maxBet
  }

  actionCall() {
    let ap = this.activePlayer
    let prevBet = ap.bet
    ap.bet += this.betToCall() - prevBet
    this.saveAction(
      ap.name,
      'call',
      ap.bet,
      ap.stack,
      this.potSize,
      'Call to '.concat(ap.bet),
      prevBet
    )
    this.incPot(ap.bet)
    this.activateNextPlayer()
  }

  betToRaise50() {
    let prevPlayerBet = this.prevUnfoldedPlayer.bet
    let minRaise = 2 * prevPlayerBet
    return minRaise
  }

  actionRaise50() {
    let ap = this.activePlayer
    let prevBet = ap.bet
    let betToRaise = this.betToRaise50()
    if (ap.bet > 0) {
      ap.incStack(ap.bet)
    }
    ap.bet = betToRaise
    this.saveAction(
      ap.name,
      'raise 50',
      ap.bet,
      ap.stack,
      this.potSize,
      'Raise 50% to '.concat(ap.bet),
      prevBet
    )
    this.incPot(ap.bet)
    this.activateNextPlayer()
  }

  betToRaise100() {
    let prevPlayerBet = this.prevUnfoldedPlayer.bet
    let totalBets = this.totalBets
    totalBets -= prevPlayerBet
    totalBets += 3 * prevPlayerBet
    return totalBets
  }

  actionRaise100() {
    let ap = this.activePlayer
    let prevBet = ap.bet
    let betToRaise = this.betToRaise100()
    if (ap.bet > 0) {
      ap.incStack(ap.bet)
    }
    ap.bet = betToRaise
    this.saveAction(
      ap.name,
      'raise 100',
      ap.bet,
      ap.stack,
      this.potSize,
      'Raise 100% to '.concat(ap.bet),
      prevBet
    )
    this.incPot(ap.bet)
    this.activateNextPlayer()
  }

  actionAllin() {
    let ap = this.activePlayer
    let prevBet = ap.bet
    let allIn = ap.stack
    ap.bet = allIn
    this.saveAction(
      ap.name,
      'allin',
      ap.bet,
      ap.stack,
      this.potSize,
      'AllIn to '.concat(ap.bet),
      prevBet
    )
    this.incPot(ap.bet)
    this.activateNextPlayer()
  }

  setPlayerBet(name, bet) {
    let p = this.getPlayerByName(name)
    if (p) {
      p.bet = bet
      this.incPot(bet)
    }
  }

  foldPlayer(name) {
    let p = this.getPlayerByName(name)
    if (p) {
      p.fold = true
    }
  }

  saveAction(player, action, bet, stack, pot, situation, prevBet = 0) {
    this.gameStory.push({
      player: player,
      action: action,
      prevbet: prevBet,
      bet: bet,
      stack: stack,
      pot: pot,
      situation: situation
    })
  }

  revertAction() {
    if (this.gameStory.length > 0) {
      let story = this.gameStory.pop()
      if (this.gameStory.length == 0) {
        this.initGame()
      } else {
        this.activatePrevPlayer()
        let player = this.activePlayer
        player.stack = story.stack
        this.decPot(story.bet)
        player.bet = story.prevbet
        if (story.action == 'fold') {
          player.fold = false
        }
      }
    }
  }

  // for VUE reactivity on changed state of game object
  incIdx() {
    this.players.map((player) => {
      player.idx = this._idx
      this._idx++
    })
  }
}

export { pokerGame }
//module.exports = pokerGame
