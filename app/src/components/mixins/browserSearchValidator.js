const _ = require('lodash')
export default class BrowserSearchValidator {
  BrowserSearchValidator() {}
  constructor() {
    this.handMacros = [/\$ds/, /\$ss/, /\$rr/]
    this.handColor = ['c', 'd', 'h', 's']
    this.handCards = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2']
  }

  get input() {
    return this._input
  }

  set input(i) {
    this._input = i
    this._macro = null
    this._hand = null
    this._ds = null
    this._ss = null
    this._rr = null
  }

  getHandPositions(arr) {
    return arr.map((card) => this.handCards.indexOf(card.toUpperCase()))
  }

  getSuitPositions(arr) {
    return arr.map((suit) => this.handColor.indexOf(suit.toLowerCase()))
  }

  validateMacro() {
    let validMacro = false
    // hand ends with : without macro definition
    if (this._input.search(/:/) != -1 && this._input.search(/\$/) == -1) {
      validMacro = false
      this._hand = this._input.replace(':', '')
    } else if (this._input.search(/\$/) != -1) {
      validMacro = false
      this.handMacros.forEach((m) => {
        if (this._input.search(m) != -1) {
          validMacro = true
          this._macro = String(m)
            .replace('\\', '')
            .replace('/', '')
            .replace('/', '')
          this._hand = this._input.replace(this._macro, '').replace(':', '')
          if (this._macro == '$ds') {
            this._ds = true
          }
          if (this._macro == '$ss') {
            this._ss = true
          }
          if (this._macro == '$rr') {
            this._rr = true
          }
        }
      })
      /** if (!validMacro) {
        throw new Error('Invalid hand Macro')
      }*/
    } else {
      validMacro = true
      this._hand = this._input
    }
    return validMacro
  }

  validateSuit() {
    if (this._hand != null && this._hand.length > 4 && this._hand.length % 2 == 0) {
      let pairs = this._hand.match(/.{1,2}(?=(.{2})+(?!.))|.{1,2}$/g)
      let suitPositions = pairs.map((card) => this.handColor.indexOf(card.slice(-1)))
      // test colors
      let correctSuitPositions = suitPositions.filter((s) => s != -1)
      if (correctSuitPositions.length == suitPositions.length) {
        return true
      } else {
        //throw new Error('Invalid colors')
        return false
      }
    } else {
      // without suits
      return true
    }
  }

  validateHand() {
    if (this._hand != null) {
      //AsAsAs
      if (this._hand.length > 2 && this._hand.length % 2 == 0) {
        let pairs = this._hand.match(/.{1,2}(?=(.{2})+(?!.))|.{1,2}$/g)
        let facePositions = pairs.map((card) =>
          this.handCards.indexOf(card.slice(0, -1).toUpperCase())
        )
        // test hands
        if (facePositions.indexOf(-1) >= 0) {
          return false
        }
        pairs.sort((a, b) => {
          return (
            this.handCards.indexOf(a[0].toUpperCase()) - this.handCards.indexOf(b[0].toUpperCase())
          )
        })
        pairs.forEach((part, index, arr) => {
          arr[index] = part.charAt(0).toUpperCase() + part.toLowerCase().slice(1)
        })
        this._hand = pairs.join('')
      } else if (this._hand.length == 3) {
        // without color definitions
        let cards = this._hand.split('')
        // checking
        let facePositions = this.getHandPositions(cards)
        if (facePositions.indexOf(-1) >= 0) {
          return false
        }
        cards.sort((a, b) => {
          console.log('validateHand')
          console.log(cards)
          console.log(a)
          console.log(b)
          console.log(this.handCards.indexOf(a[0].toUpperCase()))
          console.log(this.handCards.indexOf(b[0].toUpperCase()))
          return (
            this.handCards.indexOf(a[0].toUpperCase()) - this.handCards.indexOf(b[0].toUpperCase())
          )
        })
        console.log(cards)
        this._hand = cards.join('')
      } else {
        // AK
        this._hand.toUpperCase()
        let cards = this._hand.split('')
        // checking
        let facePositions = this.getHandPositions(cards)
        let suitPositions = this.getSuitPositions(cards)
        // -2 after sum of columns means absent in face and suits definition
        if (_.map(_.unzip([facePositions, suitPositions]), _.sum).indexOf(-2) >= 0) {
          return false
        }
        cards.sort((a, b) => {
          return (
            this.handCards.indexOf(a[0].toUpperCase()) - this.handCards.indexOf(b[0].toUpperCase())
          )
        })
        this._hand = cards.join('')
        // rainbow ?
        if (this._ds == null && this._ss == null) {
          // this._rr = true
        }
      }
    }
    return true
  }

  validate() {
    return this.validateMacro() && this.validateSuit() && this.validateHand()
  }

  cleanSuit(hand) {
    this.handColor.forEach((c) => {
      let re = new RegExp(c, 'gi')
      hand = hand.replace(re, '')
    })
    return hand.toUpperCase()
  }

  format() {
    return {
      hand: this._hand,
      rawhand: this.cleanSuit(this._hand),
      ds: this._ds,
      ss: this._ss,
      rr: this._rr
    }
  }
}

//module.exports = BrowserSearchValidator
