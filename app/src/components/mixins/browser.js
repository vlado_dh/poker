export default {
  data() {
    return {}
  },
  computed: {
    getChange: function() {
      let amount = this.potSize
      let chips = this.chipValues
      return chips.map((chip) => {
        let amountChip = Math.floor(amount / chip)
        amount -= amountChip * chip
        return [amountChip, chip]
      })
    },
    onlyNonZeroChips: function() {
      let chipArr = this.getChange
      chipArr.forEach((ch) => {
        if (ch[0] > 1) {
          for (let i = 1; i < ch[0]; i++) {
            chipArr.push([ch[0], ch[1]])
          }
        }
      })
      return chipArr.filter(function(ch) {
        return ch[0]
      })
    }
  },
  methods: {
    splitHands: function(cardSuit) {
      if (cardSuit.length == 8 && cardSuit.indexOf('[') == -1) {
        return cardSuit.match(/.{1,2}(?=(.{2})+(?!.))|.{1,2}$/g).map(function(elem) {
          return elem.replace(/[shdc]$/g, 's')
        })
      }
      // rainbow
      if (cardSuit.length == 4 && cardSuit.indexOf('[') == -1) {
        let colorPosition = ['s', 'h', 'd', 'c']
        return cardSuit.split('').map(function(elem) {
          return elem + colorPosition.pop()
        })
      }
      // [AKQT], A[KQ]T, ...
      if (cardSuit.indexOf('[') >= 0) {
        let res = null
        let regex = null
        let rep = null
        // A[KJ2]
        regex = /^([AKQJT2-9]{1})\[([AKQJT2-9]{1})([AKQJT2-9]{1})([AKQJT2-9]{1})\]$/gi
        res = cardSuit.match(regex)
        if (res) {
          rep = cardSuit.replace(regex, function(match, p1, p2, p3, p4) {
            return `${p1}s${p2}h${p3}h${p4}h`
          })
        }
        // [AJ][AJ]
        regex = /^\[([AKQJT2-9]{1})([AKQJT2-9]{1})\]\[([AKQJT2-9]{1})([AKQJT2-9]{1})\]$/gi
        res = cardSuit.match(regex)
        if (res) {
          rep = cardSuit.replace(regex, function(match, p1, p2, p3, p4) {
            return `${p1}s${p2}s${p3}h${p4}h`
          })
        }
        // [943]7
        regex = /^\[([AKQJT2-9]{1})([AKQJT2-9]{1})([AKQJT2-9]{1})\]([AKQJT2-9]{1})$/gi
        res = cardSuit.match(regex)
        if (res) {
          rep = cardSuit.replace(regex, function(match, p1, p2, p3, p4) {
            return `${p1}s${p2}s${p3}s${p4}h`
          })
        }
        // [AKQT]
        regex = /^\[([AKQJT2-9]{1})([AKQJT2-9]{1})([AKQJT2-9]{1})([AKQJT2-9]{1})\]$/gi
        res = cardSuit.match(regex)
        if (res) {
          rep = cardSuit.replace(regex, function(match, p1, p2, p3, p4) {
            return `${p1}s${p2}s${p3}s${p4}s`
          })
        }
        if (!res) {
          //[AK]QT
          regex = /^\[([AKQJT2-9]{1})([AKQJT2-9]{1})\]([AKQJT2-9]{1})([AKQJT2-9]{1})$/gi
          res = cardSuit.match(regex)
          if (res) {
            rep = cardSuit.replace(regex, function(match, p1, p2, p3, p4) {
              return `${p1}s${p2}s${p3}h${p4}d`
            })
          }
        }
        if (!res) {
          // A[KQ]T
          regex = /^([AKQJT2-9]{1})\[([AKQJT2-9]{1})([AKQJT2-9]{1})\]([AKQJT2-9]{1})$/gi
          res = cardSuit.match(regex)
          if (res) {
            rep = cardSuit.replace(regex, function(match, p1, p2, p3, p4) {
              return `${p1}s${p2}h${p3}h${p4}d`
            })
          }
        }
        if (!res) {
          //AK[QT]
          regex = /^([AKQJT2-9]{1})([AKQJT2-9]{1})\[([AKQJT2-9]{1})([AKQJT2-9]{1})\]$/gi
          res = cardSuit.match(regex)
          if (res) {
            rep = cardSuit.replace(regex, function(match, p1, p2, p3, p4) {
              return `${p1}s${p2}h${p3}d${p4}d`
            })
          }
        }
        if (rep) {
          return rep.match(/.{1,2}(?=(.{2})+(?!.))|.{1,2}$/g).map(function(elem) {
            return elem
          })
        }
      }
    },
    disableActionButtons() {
      this.backButton.visible = false
      this.callButton.visible = false
      this.foldButton.visible = false
      this.raise50Button.visible = false
      this.raise100Button.visible = false
      this.allinButton.visible = false
      this.searchInputDisable = true
      this.categoryListDisable = true
    },
    enableActionButtons(eb) {
      if (eb.indexOf('call') != -1) {
        this.callButton.visible = true
      }
      if (eb.indexOf('fold') != -1) {
        this.foldButton.visible = true
      }
      if (eb.indexOf('raise50') != -1) {
        this.raise50Button.visible = true
      }
      if (eb.indexOf('raise100') != -1) {
        this.raise100Button.visible = true
      }
      if (eb.indexOf('allin') != -1) {
        this.allinButton.visible = true
      }
      this.searchInputDisable = false
      this.categoryListDisable = false
    }
  }
}
