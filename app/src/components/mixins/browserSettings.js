/**
 * Profil: (PLO50, PLO500, PLO500), tabuľka profiles
 * Stack: (50,100,150,200), tabuľkas tack. Na začiatku budú mať všeci hráči rovnaký vklad.
 * Number of players: 2 - 6 (dáta sú len pre 6) parameter bude readonly: 6
 */
export default {
  data() {
    return {
      currentSettings: {
        profile: 'PLO500',
        stack: 100,
        numberOfPlayers: 6,
        stackFormat: 'bb'
      },
      lib: {
        browserProfileOptions: ['PLO50', 'PLO500', 'PLO5000'],
        browserStackOptions: [50, 100, 150, 200],
        browserNumberOfPlayersOptions: [
          {
            label: '2',
            value: 2,
            disable: true
          },
          {
            label: '3',
            value: 3,
            disable: true
          },
          {
            label: '4',
            value: 4,
            disable: true
          },
          {
            label: '5',
            value: 5,
            disable: true
          },
          {
            label: '6',
            value: 6,
            disable: false
          }
        ]
      }
    }
  },
  methods: {
    getBrowserProfileOptions() {
      return this.lib.browserProfileOptions
    },
    getBrowserStackOptions() {
      return this.lib.browserStackOptions
    },
    getBrowserNumberOfPlayersOptions() {
      return this.lib.browserNumberOfPlayersOptions
    }
  }
}
