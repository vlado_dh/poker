// const _ = require('lodash')

export default class BrowserSearchValidatorRK {
  BrowserSearchValidatorRK() { }
  constructor(input) {
    this.input = input

    this.handMacros = ['$ds', '$ss', '$rr']
    this.handColors = ['c', 'd', 'h', 's']
    this.handCards = ['a', 'k', 'q', 'j', 't', '9', '8', '7', '6', '5', '4', '3', '2']

    this.cards = [] /* { card: 'A', color: 'd' } */
    this._cardString = null
    this._hasCards = null
    this._hasValidCards = null

    this.macros = []
    this._macroString = null
    this._hasMacros = null
    this._hasValidMacros = null

    this.parse()
  }

  hasCards() {
    return !!this._hasValidCards
  }

  hasMacros() {
    return !!this._hasValidMacros
  }

  parse() {
    this.type = null
    if (!this.input.length) {
      this.type = 1
      this.cards = []
      this._cardString = ''
      this._hasCards = false
      this._hasValidCards = true
      this.macros = []
      this._macroString = ''
      this._hasMacros = false
      this._hasValidMacros = true
    } else if (this.input.indexOf(':') != -1) {
      this.type = 2
      this._hasCards = true
      this._hasMacros = true
      let parts = this.input.split(':', 2)
      this._cardString = parts[0]
      this._macroString = parts[1]
    } else {
      this.type = 3
      this._cardString = this._macroString = this.input
    }

    if ([2, 3].indexOf(this.type) !== -1) {
      this.validateCards(this.type !== 3)
      this.validateMacros()
    }
  }

  hasValidCards() {
    return this._hasValidCards
  }

  hasValidMacros() {
    return this._hasValidMacros
  }

  charIsCard(c) {
    c = c.toLowerCase()
    return this.handCards.indexOf(c) !== -1
  }

  charIsColor(c) {
    c = c.toLowerCase()
    return this.handColors.indexOf(c) !== -1
  }

  validateCards(emptyIsError) {
    if (typeof emptyIsError !== 'undefined') emptyIsError = true
    try {
      for (let x = 0; x < this._cardString.length; x++) {
        let c = this._cardString.charAt(x)
        if (this.charIsCard(c)) {
          this.cards.push({ card: c, color: null })
        } else if (this.charIsColor(c)) {
          if (!this.cards.length) throw 'colorToMissingCard'
          else if (this.cards[this.cards.length - 1].color !== null)
            throw 'duplicateColorAssignment'
          this.cards[this.cards.length - 1].color = c
        } else {
          throw 'wrongSymbol'
        }
      }
      if (!this.cards.length && emptyIsError) throw 'noCards'
      if (this.cards.length > 4) throw 'tooManyCards'
    } catch (e) {
      this._hasValidCards = false
      return false
    }
    this._hasValidCards = true
    return true
  }

  orderCards() {
    this.cards.sort((a, b) => {
      return this.handCards.indexOf(a.card) - this.handCards.indexOf(b.card)
    })
  }

  validateMacros() {
    try {
      let r = this._macroString.match(/(\$.{2})/gi)
      if (r) {
        r.forEach((o) => {
          if (o == '$ss') {
            this._ss = true
            if (this.macros.indexOf('ss') === -1) this.macros.push('ss')
          } else if (o == '$ds') {
            this._ds = true
            if (this.macros.indexOf('ds') === -1) this.macros.push('ds')
          } else if (o == '$rr') {
            this._rr = true
            if (this.macros.indexOf('r') === -1) this.macros.push('rr')
          } else {
            throw 'unknownMacro'
          }
        })
      }
    } catch (e) {
      this._hasValidMacros = false
      return false
    }
    this._hasValidMacros = true
    return true
  }

  formatHand() {
    if (!this.hasValidCards()) return ''
    let s = ''
    this.cards.forEach(
      (item) => (s += item.card.toUpperCase() + (item.color != null ? item.color : ''))
    )
    return s
  }

  formatRawhand() {
    if (!this.hasValidCards()) return ''
    let s = ''
    this.cards.forEach((item) => (s += item.card.toUpperCase()))
    return s
  }

  validate() {
    console.log({
      hasValidCards: this.hasValidCards(),
      hasValidMacros: this.hasValidMacros()
    })
    return this.hasValidCards() && this.hasValidMacros()
  }

  format() {
    this.orderCards()
    return {
      hand: this.formatHand(),
      rawhand: this.formatRawhand(),
      ds: this._ds,
      ss: this._ss,
      rr: this._rr
    }
  }
}
