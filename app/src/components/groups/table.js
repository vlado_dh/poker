// @ts-nocheck
import { QIcon, QSelect, QTable, QTd, QTh, QTooltip, QTr } from 'quasar'

export default {
  QTable,
  QTh,
  QTr,
  QTd,
  QSelect,
  QIcon,
  QTooltip
}
