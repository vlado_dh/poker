// @ts-nocheck
import { QTabs, QTab} from 'quasar'

export default {
  QTabs,
  QTab
}
