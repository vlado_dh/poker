import { default as feathers, HookContext } from '@feathersjs/feathers';

let splitCards = function (c: string) {
  let a: string[] = [], d: string[] = []
  c.split('').forEach((b: string) => {
    if (b == '%') return false
    if (a.indexOf(b) !== -1) {
      d[a.indexOf(b)] += b
    } else {
      a.push(b)
      d.push(b)
    }
  })
  return d
}

export default {
  before: {
    all: [],
    find: [async (context: HookContext) => {
      let sequelize = context.app.get('sequelizeClient')

      type qParamsInterface = {
        [key: string]: any
      }
      let qParams: qParamsInterface = {}
      console.log(context.params.query)
      let q = ''
      let qa = 'SELECT t.*, hc.combinations FROM preflop_hands t \
        JOIN hand_categories hc ON hc.hand = t.hand \
        WHERE 1=1';
      let qc = 'SELECT COUNT(t.*) FROM preflop_hands t WHERE 1=1';
      let ql = ''

      let pc = ''
      let pc2 = ''
      let pParams: qParamsInterface = {}
      let p2Params: qParamsInterface = {}

      /*
      select * from preflop_hands WHERE hand IN (
        select hand from hand_categories WHERE rawhand LIKE '%A%' AND ds = true
      ) AND "action" = 'CALL' LIMIT 100
      */
      let qp = context.params.query || {}
      if (qp) {
        if (qp.situation_id) {
          q += ' AND t.situation_id = :situation_id'
          qParams['situation_id'] = qp.situation_id
          pc += ' AND t.situation_id = :situation_id'
          pParams['situation_id'] = qp.situation_id
          pc2 += ' AND t.situation_id = :situation_id'
          p2Params['situation_id'] = qp.situation_id
        }
        if (qp.action) {
          q += ' AND t.action = :action'
          qParams['action'] = qp.action
          pc += ' AND t.action = :action'
          pParams['action'] = qp.action
        }
        if (qp.weight) {
          if (typeof qp.weight === 'object' && typeof qp.weight['$gte'] !== 'undefined') {
            // q += ' AND t.weight != 0'
            q += ' AND t.weight >= 0.05'
            // pc += ' AND t.weight >= 0.05'
            // pc2 += ' AND t.weight >= 0.05'
          } else {
            q += ' AND t.weight = :weight'
          }
        }
        if (qp.rawhand || qp.category || qp.ss || qp.ds || qp.rainbow) {
          q += ' AND t.hand IN ( \
            SELECT hc2.hand FROM hand_categories hc2 WHERE 1=1'
          pc += ' AND t.hand IN ( SELECT hc2.hand FROM hand_categories hc2 WHERE 1=1'
          pc2 += ' AND t.hand IN ( SELECT hc2.hand FROM hand_categories hc2 WHERE 1=1'
          if (qp.rawhand && typeof qp.rawhand['$like'] !== 'undefined') {
            splitCards(qp.rawhand['$like']).forEach((part: any, idx: any) => {
              if (part == '%') return true
              q += ' AND hc2.rawhand LIKE :rawhand' + idx
              qParams['rawhand' + idx] = '%' + part + '%'
              pc += ' AND hc2.rawhand LIKE :rawhand' + idx
              pParams['rawhand' + idx] = '%' + part + '%'
              pc2 += ' AND hc2.rawhand LIKE :rawhand' + idx
              p2Params['rawhand' + idx] = '%' + part + '%'
            })
          } else if (qp.rawhand) {
            splitCards(qp.rawhand).forEach((part: any, idx: any) => {
              if (part == '%') return true
              q += ' AND hc2.rawhand LIKE :rawhand' + idx
              qParams['rawhand' + idx] = '%' + part + '%'
              pc += ' AND hc2.rawhand LIKE :rawhand' + idx
              pParams['rawhand' + idx] = '%' + part + '%'
              pc2 += ' AND hc2.rawhand LIKE :rawhand' + idx
              p2Params['rawhand' + idx] = '%' + part + '%'
            })
          }
          if (qp.category) {
            q += ' AND hc2.category = :category'
            qParams['category'] = qp.category
            pc += ' AND hc2.category = :category'
            pParams['category'] = qp.category
            pc2 += ' AND hc2.category = :category'
            p2Params['category'] = qp.category
          }
          if (qp.ss) {
            q += ' AND hc2.ss = :ss'
            qParams['ss'] = qp.ss
            pc += ' AND hc2.ss = :ss'
            pParams['ss'] = qp.ss
            pc2 += ' AND hc2.ss = :ss'
            p2Params['ss'] = qp.ss
          }
          if (qp.ds) {
            q += ' AND hc2.ds = :ds'
            qParams['ds'] = qp.ds
            pc += ' AND hc2.ds = :ds'
            pParams['ds'] = qp.ds
            pc2 += ' AND hc2.ds = :ds'
            p2Params['ds'] = qp.ds
          }
          if (qp.rainbow) {
            q += ' AND hc2.rainbow = :rainbow'
            qParams['rainbow'] = qp.rainbow
            pc += ' AND hc2.rainbow = :rainbow'
            pParams['rainbow'] = qp.rainbow
            pc2 += ' AND hc2.rainbow = :rainbow'
            p2Params['rainbow'] = qp.rainbow
          }
          q += ')'
          pc += ')'
          pc2 += ')'
        }
        ql += ' ORDER BY t.ev DESC, t.weight DESC'
        ql += (qp['$skip'] ? ' OFFSET ' + qp['$skip'] : '') + ' LIMIT ' + qp['$limit']
      }

      let pq = 'SELECT a.dividend, b.divisor, a.dividend/b.divisor AS percentage FROM\
        (SELECT SUM(t.weight * hc.combinations) AS dividend \
          FROM preflop_hands t \
          JOIN hand_categories hc ON hc.hand = t.hand \
          WHERE 1=1' + pc + ') AS a\
        , (SELECT SUM(t.weight * hc.combinations) AS divisor \
          FROM preflop_hands t \
          JOIN hand_categories hc ON hc.hand = t.hand \
          WHERE 1=1' + pc2 + ') AS b'

      console.log(pq)
      console.log(qa + q + ql)
      console.log(pParams)
      console.log(p2Params)
      return sequelize
        .query(qa + q + ql, { replacements: qParams, type: sequelize.QueryTypes.SELECT })
        .then((projects: any) => {
          context.result = {
            limit: context.params.query && context.params.query['$limit'] ? context.params.query['$limit'] : 0,
            skip: context.params.query && context.params.query['$skip'] ? context.params.query['$skip'] : 0,
            total: 0,
            data: projects
          }
          return
        }).then(() => {
          if (qp.rawhand || qp.category || qp.ss || qp.ds || qp.rr) {
            return sequelize.query(pq, { replacements: pParams, type: sequelize.QueryTypes.SELECT })
              .then((ret: any) => {
                context.result.additional = {
                  dividend: ret[0].dividend,
                  divisor: ret[0].divisor,
                  percentage: ret[0].percentage
                }
              }).catch((err: any) => { console.log(err) })
          }
          return
        }).then(() => {
          return sequelize.query(qc + q, { replacements: qParams, type: sequelize.QueryTypes.SELECT })
            .then((projectCount: any) => {
              context.result.total = parseInt(projectCount[0].count)
            }).catch((err: any) => { console.log(err) })
        })

      /**
       * PATRIKOVA VERZIA - SEARCH:
        let query: any = context.params.query;
        if ("search" in query)
        {
          let rawhandParameter: string = query.search;
          delete query.search
          // console.log('hook01, mame search', query);
          const selectIn =" (select rawhand from hand_categories where rawhand like '" + rawhandParameter + "') "
          console.log(selectIn);
          let it: any;
          const items2 = context.service.find({
            query: {
              rawhand: {
                $in:  sequelize.literal(selectIn)
              }
            }
          });
          //.then(items2 => context.data = items2.data);
          const handCategoriesSearchData = await items2.then(
            (handCategoriesSearchData) => {
              return handCategoriesSearchData
            }
          )
       */

      // return sequelize
      //   .query(qa + q + ql, { replacements: qParams, type: sequelize.QueryTypes.SELECT })
      //   .then((projects: any) => {
      //     sequelize.query(qc + q, { replacements: qParams, type: sequelize.QueryTypes.SELECT })
      //       .then((projectCount: any) => {
      //         console.log(qc + q)
      //         console.log(projectCount[0].count)
      //         context.result = {
      //           limit: context.params.query && context.params.query['$skip'] ? context.params.query['$skip'] : 0,
      //           skip: context.params.query && context.params.query['$skip'] ? context.params.query['$skip'] : 0,
      //           total: parseInt(projectCount[0].count),
      //           data: projects
      //         }
      //       }).catch((err: any) => { console.log(err) })
      //   })


      // context.result = {
      //   limit: 100,
      //   skip: 0,
      //   total: 1,
      //   data: [{
      //     action: "RAISE100",
      //     ev: 8339.04,
      //     hand: "[AJ][AJ]",
      //     id: "24220",
      //     interesting: false,
      //     weight: 1
      //   }]
      // }
      // console.log(context);
      // context.result.total = context.app.get('sequelizeClient')
      //   .query('SELECT * FROM projects')
      //   .then(projects => {
      //   })
      // return context;
    }],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [async (context: HookContext) => {
      // console.log(context);
      // context.result.total = context.app.get('sequelizeClient')
      //   .query('SELECT * FROM projects')
      //   .then(projects => {
      //   })
      return context;
    }],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
