// Initializes the `preflop_hands` service on path `/preflop-hands`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PreflopHands } from './preflop_hands.class';
import createModel from '../../models/preflop_hands.model';
import hooks from './preflop_hands.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'preflop-hands': PreflopHands & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate: {
      default: 100,
      max: 500
    }
  };

  // Initialize our service with any options it requires
  app.use('/preflop-hands', new PreflopHands(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('preflop-hands');

  service.hooks(hooks);
}
