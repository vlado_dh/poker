import { Application } from '../declarations'
import users from './users/users.service'
import hands from './hands/hands.service'
import preflopHands from './preflop_hands/preflop_hands.service';
import situations from './situations/situations.service';
import profiles from './profiles/profiles.service';
import stacks from './stacks/stacks.service';
import preflopPercentage from './preflop_percentage/preflop_percentage.service';
import handCategories from './hand_categories/hand_categories.service';
import vPreflopHands from './v_preflop_hands/v_preflop_hands.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function(app: Application) {
  app.configure(users)
  app.configure(hands)
  app.configure(preflopHands);
  app.configure(situations);
  app.configure(profiles);
  app.configure(stacks);
  app.configure(preflopPercentage);
  app.configure(handCategories);
  app.configure(vPreflopHands);
}
