// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes } from 'sequelize';
import { Application } from '../declarations';

export default function (app: Application) {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const preflopHands = sequelizeClient.define('preflop_hands', {
    hand: {
      type: DataTypes.TEXT,
      allowNull: false
    },

    weight: {
      type: DataTypes.REAL,
      allowNull: false
    },

    ev: {
      type: DataTypes.REAL,
      allowNull: false
    },

    action: {
      type: DataTypes.TEXT,
      allowNull: false
    },

    interesting: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },

    situation_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  },
  {timestamps: false}
);

  // eslint-disable-next-line no-unused-vars
  (preflopHands as any).associate = function (models: any) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return preflopHands;
}
