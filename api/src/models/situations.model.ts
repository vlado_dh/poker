// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes } from 'sequelize';
import { Application } from '../declarations';

export default function (app: Application) {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const situations = sequelizeClient.define('situations', {

    id:{
      type:DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'situation_id'
    },

    profile: {
      type: DataTypes.TEXT,
      allowNull: true
    },

    stack:{
      type:DataTypes.SMALLINT,
      allowNull: true
    },

    players_cnt:{
      type:DataTypes.SMALLINT,
      allowNull: true
    },

    postflop_code: {
      type: DataTypes.TEXT,
      allowNull: true
    },

    preflop_code: {
      type: DataTypes.TEXT,
      allowNull: true
    },

    flop: {
      type: DataTypes.TEXT,
      allowNull: true
    },

    turn: {
      type: DataTypes.TEXT,
      allowNull: true
    },

    river: {
      type: DataTypes.TEXT,
      allowNull: true
    },
  },
  /*
   {

    hooks: {
      beforeCount(options: any) {
        options.raw = true;
      }
    }

  },
  */
{timestamps: false},
);

  // eslint-disable-next-line no-unused-vars
  (situations as any).associate = function (models: any) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return situations;
}
